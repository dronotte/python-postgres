import psycopg2
import traceback
import sys

class Postgres():
    # Конструктор
    def __init__(self, db_name, db_user, db_password, db_host, db_port):
        self.db_name = db_name
        self.db_user = db_user
        self.db_password = db_password
        self.db_host = db_host
        self.db_port = db_port
        self.connection = self.create_connection()
    # Создание соединения
    def create_connection(self):
        self.connection = None
        try:
            self.connection = psycopg2.connect(
                database=self.db_name,
                user=self.db_user,
                password=self.db_password,
                host=self.db_host,
                port=self.db_port,
            )
            print("Connection to PostgreSQL DB successful")
        except Exception as e:
            print(f"The error '{e}' occurred")
        return self.connection



    # Список запросов
    read_query_schemas = "select schema_name from information_schema.schemata;"
    read_query_check_schemas = "SELECT schema_name from information_schema.schemata WHERE schema_name = '{0}';"
    read_query_tables = "SELECT table_name FROM information_schema.tables WHERE table_schema = '{0}';"
    read_query_check_tables = "SELECT table_name from information_schema.tables WHERE table_name = '{0}';"
    read_query_roles = "SELECT rolname FROM pg_roles;"
    read_query_check_roles = "SELECT rolname FROM pg_roles WHERE rolname = '{0}';"
    # {0} - GRANT/REVOKE
    # {1} - USAGE/CREATE
    # {2} - SCHEMA
    # {3} - ROLE
    # {4} - TO/FROM (grant/revoke)
    # template: GRANT USAGE ON SCHEMA aisbpek TO "username";
    execute_query_schema = '{0} {1} ON SCHEMA {2} {4} "{3}";'
    # {0} - GRANT/REVOKE
    # {1} - SELECT | INSERT | UPDATE | DELETE | TRUNCATE | REFERENCES | TRIGGER
    # {2} - SCHEMA
    # {3} - TABLE
    # {4} - ROLE
    # {5} - TO/FROM (grant/revoke)
    #template: GRANT SELECT ON TABLE public.table_1 TO test_user;
    execute_query_table = '{0} {1} ON {2}.{3} {5} "{4}";'
    # Методы

    # Методы для работы с кортежами

    def print_uncort_uncort(self,cort):
        for units in cort:
            for unit in units:
                print(unit)

    def print_uncort(self,cort):
        for unit in cort:
            print(unit)

    # Опишем метод доставания схемы из кортежа кортежей. - Формируем не список
    # кортежей, а кортеж.

    def list_of_corts_to_cort(self, list_of_corts):
        corts = []
        for cort in list_of_corts:
            for dot in cort:
                corts.append(dot)
                #print(dot)
        return corts

    def list_of_corts(self, list_of_corts):
        corts = []
        for cort in list_of_corts:
            corts.append(cort)
            #print(dot)
        return corts

    # Метод, создающий базу данных.
    def create_database(self, query):
        self.connection.autocommit = True
        cursor = self.connection.cursor()
        try:
            cursor.execute(query)
            print("Query executed successfully")
        except Exception as e:
            print(f"The error '{e}' occurred")

    # Метод, выполняющий запрос на измеенение.
    def execute_query(self, query):
        self.connection.autocommit = True
        self.cursor = self.connection.cursor()
        try:
            self.cursor.execute(query)
            print("Query executed successfully")
        except Exception as e:
            print(f"The error '{e}' occurred")

    # Метод выполнения запроса на чтение.
    def execute_read_query(self, query):
        try:
            cursor = self.connection.cursor()
        except:
            print('Проверь подключение (доступ) к бд')
            return
        result = None
        try:
            cursor.execute(query)
            result = cursor.fetchall()
            return result
        except Exception as e:
            print(f"The error '{e}' occurred")

    # Метод, возвращающий все схемы в базе данных.
    def schema_list(self):
        schemas = self.execute_read_query(self.read_query_schemas)
        #print('Список схем в базе данных',self.db_name, 'таков')
        #for schema in schemas:
        #    print(schema)
        return schemas

    #Метод проверки введенного списка схем
    def check_schema(self, selected_schemas):
        splitted_schemas = selected_schemas.split()
        schemas = []
        for schema in splitted_schemas:
            try:
                #print('Выполняю запрос: ',query)
                a = self.execute_read_query(self.read_query_check_schemas.format(schema))
                #print(a)
                schemas.append(schema)
            except Exception as e:
                print(f"The error '{e}' occurred")
        return schemas



    # Метод, выводящий список таблиц в выбранных схемах.
    def table_list(self, schema):
        try:
            #print(schema)
            tables = self.list_of_corts_to_cort(self.execute_read_query(self.read_query_tables.format(schema)))
            print('В схеме {0}'.format(schema),'есть такие таблицы:')
            self.print_uncort(tables)
        except Exception as e:
            print(f"The error '{e}' occurred")
        return tables

    # В связи с тем, что метод table_list возвращает при введенном значении
    # * в любой схеме принимает значение имени схемы в качестве кортежа,
    # разбирает его в виде (p u b l i c) и для каждого элемента пытается прочитать
    # список таблиц, необходимо создать метод, считывающий его не как кортеж, а как
    # одиночное имя схемы
    def one_schema_table_list(self, schema):
        selected_tables = self.list_of_corts_to_cort(self.execute_read_query(self.read_query_tables.format(schema)))
        #print(selected_tables)
        #splitted_tables = selected_tables.split()
        tables = {}
        for table in selected_tables:
            try:
                a = self.execute_read_query(self.read_query_check_tables.format(table))
                tables.update({table: schema})
            #print(tables)
            except Exception as e:
                print(f"The error '{e}' occurred")
        return tables

    # Метод проверки введенного списка таблиц
    def check_tables(self, selected_tables,schema):
        splitted_tables = selected_tables.split()
        tables = {}
        for table in splitted_tables:
            try:
                #print('Выполняю запрос: ',query)
                a = self.execute_read_query(self.read_query_check_tables.format(table))
                #print(a)
                tables.update({table: schema})
            except Exception as e:
                print(f"The error '{e}' occurred")
        return tables




    # Выводим список ролей
    def role_list(self):
        roles = self.execute_read_query(self.read_query_roles)
        #print('Список схем в базе данных',self.db_name, 'таков')
        #for schema in schemas:
        #    print(schema)
        return roles

    # Проверяем введенные роли
    def check_roles(self, selected_roles):
        splitted_roles = selected_roles.split()
        roles = []
        for role in splitted_roles:
            try:
                a = self.execute_read_query(self.read_query_check_roles.format(role))
                roles.append(role)
            except Exception as e:
                print(f"The error '{e}' occurred")
        return roles

    # Пишем SQL запрос на изменение прав доступа

    def edit_privelegies_schema(self,question_schema_grant_revoke,
    question_schema_usage_create,
    question_table, schemas, tables, roles, to_from):
        for schema in schemas:
            for table in tables:
                for role in roles:
                    if schema == tables.get(table):
                        print('Выполняется запрос')
                        print()
                        print(self.execute_query_schema.format(question_schema_grant_revoke,
                        question_schema_usage_create,schema,role,to_from))
                        self.execute_query(self.execute_query_schema.format(question_schema_grant_revoke,
                        question_schema_usage_create,schema,role,to_from))
                        print(self.execute_query_table.format(question_schema_grant_revoke,
                        question_table,schema,table,role,to_from))
                        self.execute_query(self.execute_query_table.format(question_schema_grant_revoke,
                        question_table,schema,table,role,to_from))

    def write_file(self,sql):
        f = open('code.sql','a')
        try:
            sql = '{0}\n'.format(sql)
            f.write(sql)
        except:
            f.close()

    def read_file(self):
        f = open('code.sql','r')
        try:
            for line in f:
                f.read(line)
        except:
            f.close()


    def export_privelegies_schema(self,question_schema_grant_revoke,
    question_schema_usage_create,
    question_table, schemas, tables, roles,to_from):
        for role in roles:
            for schema in schemas:
                self.write_file(self.execute_query_schema.format(question_schema_grant_revoke,
                question_schema_usage_create,schema,role,to_from))
                for table in tables:
                    if schema == tables.get(table):
                        print('Генерирую запрос')
                        print()

                        self.write_file(self.execute_query_table.format(question_schema_grant_revoke,
                        question_table,schema,table,role,to_from))
        print()
        print('Был создан файл "./code.sql", в котором хранится скрипт для выполнения запросы')
        print('Содержимое файла:')
        print()
        self.read_file()



def select_schemas(selected_schemas):
    if selected_schemas == '*':
        selected_schemas = postgres.schema_list()
        print('Вы выбрали все схемы:')
        #print(type(selected_schemas))
        schemas = postgres.list_of_corts_to_cort(postgres.schema_list())
        #print(schemas)
        '''
        schemas = []
        for schema_cort in selected_schemas:
            for schema in schema_cort:
                print(schema)
                schemas.append(schema)
        #print(schemas)
        '''
    else:
        schemas = postgres.check_schema(selected_schemas)
        print('Выбранные схемы:')
        #print(postgres.check_schema(selected_schemas))
        #for schema in schemas:
        #    print(schema)
    return schemas
def read_keys_of_dict(dict):
    for key in dict:
        print(key)
def select_tables(schemas):
    tables = {}
    for schema in schemas:
        postgres.table_list(schema)
        print('Введите необходимые таблицы для обработки в схеме {0}'.format(schema))
        print('Вводить список таблиц рекомендуется через пробел')
        print('Например: table_1 table_2')
        print('Также можно ввести знак * для затрагивания всех таблиц в схеме')
        print()
        selected_tables = input()
        if selected_tables == '*':
            selected_tables = (postgres.one_schema_table_list(schema))
            tables.update(selected_tables)
            #read_keys_of_dict(tables)
        else:
            selected_tables = postgres.check_tables(selected_tables,schema)
            tables.update(selected_tables)
            #print(tables)
    #tables = postgres.list_of_corts_to_cort(tables)
    return tables


def select_privelegies():
    print('Хочешь дать привелегии или забрать?')
    print('Напиши GRANT, если собираешься дать привелегии')
    print('Напиши REVOKE, если собираешься отнять привелегии')
    print()
    question_schema_grant_revoke = input()
    i = 0
    while i != 1:
        if question_schema_grant_revoke == 'GRANT':
            i = 1
            break
        elif question_schema_grant_revoke == 'REVOKE':
            i = 1
            break
        else: print('Введи GRANT или REVOKE')
    if question_schema_grant_revoke == 'GRANT':
        to_from = 'TO'
    else:
        to_from = 'FROM'
    print()
    print('Какие права мы будем давать/забирать? USAGE CREATE')
    print('Если и то и то, напиши их через пробел')
    print()
    question_schema_usage_create = input()
    print()
    print('Теперь поговорим о таблицах')
    print('Введи, какие права забрать/отдать')
    print('Список прав:')
    print('SELECT | INSERT | UPDATE | DELETE | TRUNCATE | REFERENCES | TRIGGER')
    print('Напиши их через пробел (если несколько)')
    print()
    dots = ','
    question_table_str = input()
    question_table_dic = question_table_str.split(' ')
    question_table = dots.join(question_table_dic)
    print(question_table)

    # Создается новый объект класса Postgres для внесения изменений
    # Данная библиотека не дает возможность подключаться для чтения и для
    # изменения с одним объектом класса.
    postgres1 = Postgres(postgres.db_name,postgres.db_user,postgres.db_password,
                        postgres.db_host,postgres.db_port)
    #postgres1 = Postgres("postgres", "root", "secretpassword", "127.0.0.1", "5432")
    print('Выполнить запрос? Ответь да, если нужно выполнить \
        код; ответь нет, если нужно только сгенерировать')
    que = input()
    if que == 'да':
        postgres1.edit_privelegies_schema(question_schema_grant_revoke,
        question_schema_usage_create,
        question_table, schemas, tables, roles, to_from)
    elif que == 'нет':
        postgres1.export_privelegies_schema(question_schema_grant_revoke,
        question_schema_usage_create,
        question_table, schemas, tables, roles, to_from)
        return





try:
    postgres = Postgres(sys.argv[1], sys.argv[2], sys.argv[3],
    sys.argv[4], sys.argv[5])
except Exception as e:
    print(e)
    print('Usage: app.py postgres user password host port')
    sys.exit()

print('Список схем в базе данных',postgres.db_name, 'таков')
schema_list = postgres.schema_list()
postgres.print_uncort((postgres.list_of_corts_to_cort(postgres.schema_list())))

print('Введите необходимые схемы для обработки')
print('Вводить список схем рекомендуется через пробел')
print('Например: pg_toast pg_catalog')
print('Также можно ввести знак * для затрагивания всех схем')
print()
selected_schemas = input()
print()
schemas = select_schemas(selected_schemas)
print('Выбранные схемы')
postgres.print_uncort(schemas)
print()
print('Приведем список таблиц в выбранных схемах')
print()
#postgres.table_list(schemas)
tables = []
tables = select_tables(schemas)
print()
print('Общий список выбранных таблиц')
# Соберем все таблицы из разных схем в один кортеж
read_keys_of_dict(tables)
print()
print('Список ролей:')
print()
postgres.print_uncort_uncort(postgres.role_list())
print()
print('А теперь выберем роли, с которыми будем работать')
print()
print('Введите необходимые роли для обработки')
print('Вводить список ролей рекомендуется через пробел')
print('Например: user_1 user_2')
print()
selected_roles = input()
print()
roles = postgres.check_roles(selected_roles)
print('Выбранные роли:')
postgres.print_uncort(roles)
print()
select_privelegies()
